<img src="images/readme/header-small.jpg" >

# A. Préparatifs <!-- omit in toc -->

Vous commencez maintenant à avoir l'habitude, je ne rentrerais donc pas dans les détails mais voici les différentes étapes pour le lancement du projet en mode [TL;DR](https://en.wiktionary.org/wiki/tl;dr)

1. **Créez un fork de ce TP sur https://gitlab.univ-lille.fr/js/tp5/-/forks/new**

	- Choisissez de placer le fork dans votre profil utilisateur et placez le **en mode "privé"**
	- Ajoutez en tant que "reporter" votre encadrant de TP (`@nicolas.anquetil`, `@patricia.everaere-caillier` ou `@thomas.fritsch`)

2. **Tapez dans un terminal :**
	```bash
	mkdir ~/tps-js
	git clone https://gitlab.univ-lille.fr/<votre-username>/tp5.git ~/tps-js/tp5
	codium ~/tps-js/tp5
	```
3. **Puis dans 3 terminaux splittés de VSCodium :**
	```bash
	npm i && npm run watch
	```

	> _**NB :** `npm i ...` est un **raccourci** pour `npm install ...` et l'opérateur `&&` permet de chaîner 2 commandes_

	```bash
	java -jar pizzaland-jar-with-dependencies.jar
	```

	> _**NB :** à lancer dans le dossier où vous aviez téléchargé le serveur REST [lors du précédent TP](https://gitlab.univ-lille.fr/js/tp4/-/blob/master/B-ajax.md#b3-appeler-une-api-restjson-en-get)_

	puis enfin :
	```bash
	npx serve -s -l 8000
	```

	> _**NB :** **si vous souhaitez plus de précisions** sur les commandes précédentes et l'installation  / configuration du projet, vous pouvez vous référer au chapitre [A. Préparatifs](https://gitlab.univ-lille.fr/js/tp4/-/blob/master/A-preparatifs.md) du précédent TP ou simplement demander de l'aide à votre professeur_ 😄

Le résultat attendu est le suivant :

<img src="images/readme/pizzaland-00.jpg" >


## Étape suivante <!-- omit in toc -->
Si la compilation fonctionne, vous pouvez passer à l'étape suivante : [B. jQuery : les bases](B-jquery-bases.md)